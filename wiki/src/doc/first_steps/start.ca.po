# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2021-12-22 02:12+0000\n"
"PO-Revision-Date: 2023-10-22 12:11+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Starting Tails\"]]\n"
msgstr "[[!meta title=\"Iniciar Tails\"]]\n"

#. type: Plain text
msgid "To learn how to start Tails, refer to our installation instructions on:"
msgstr ""
"Per saber com iniciar Tails, consulteu les nostres instruccions d'instal·"
"lació a:"

#. type: Bullet: '  - '
msgid "[[Starting Tails on PC|pc]]"
msgstr "[[Iniciar Tails al PC|pc]]"

#. type: Bullet: '  - '
msgid "[[Starting Tails on Mac|mac]]"
msgstr "[[Iniciar Tails a Mac|mac]]"

#~ msgid "[[Starting a Mac on a DVD|install/mac/dvd#start-dvd]]"
#~ msgstr "[[Starten von DVD mit einem Mac|install/mac/dvd#start-dvd]]"
